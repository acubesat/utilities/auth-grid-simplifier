Welcome to SimpleGrid documentation!
===========================================================

.. toctree::
  :maxdepth: 2
  :caption: Getting Started
  
  ../../README

.. toctree::
   :maxdepth: 2
   :caption: Contents:


   
.. PYTHON_MODULES_LIST_END

.. toctree::
   :maxdepth: 2
   :caption: Markdown

   marTest

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

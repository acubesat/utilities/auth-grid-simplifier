import os
import logging
from stat import S_ISDIR
from PyQt5 import QtCore
from paramiko import SSHClient, ssh_exception, AutoAddPolicy


class SSHHandler(QtCore.QObject):
    connectionStatusSignal = QtCore.pyqtSignal(str, name='ConnectionStatusSignal')
    clientConnectionSignal = QtCore.pyqtSignal(str, str, str, name='clientConnectionSignal')
    closeClientConnectionSignal = QtCore.pyqtSignal(name='CloseClientConnectionSignal')

    sendFileSignal = QtCore.pyqtSignal(str, str, name='sendFileSignal')
    sendFilesSignal = QtCore.pyqtSignal(str, str, name='sendFilesSignal')
    commandExecutionSignal = QtCore.pyqtSignal(str, name='commandExecutionSignal')

    errorDialogMessageSignal = QtCore.pyqtSignal(str, name='errorDialogMessageSignal')
    jobInsertionSignal = QtCore.pyqtSignal(list, str, name='jobInsertionSignal')

    def __init__(self, cfg_data):
        super(SSHHandler, self).__init__(parent=None)
        self.ssh_client = SSHClient()  # Create the instance of the SSH client

        # Add known hosts and also add other hosts
        self.ssh_client.load_system_host_keys()
        self.ssh_client.set_missing_host_key_policy(AutoAddPolicy())

        self.cfg_data = cfg_data
        self.logger = logging.getLogger(__name__)  # Create the logger for the file

        # Signal connections
        self.clientConnectionSignal.connect(self.connect_client)
        self.sendFilesSignal.connect(self.send_files)
        self.sendFileSignal.connect(self.send_file)
        self.commandExecutionSignal.connect(self.command_execution)
        self.closeClientConnectionSignal.connect(self.close)

    @QtCore.pyqtSlot(str, str, str, name='SSHConnectionSignal')
    def connect_client(self, username: str, password: str, priv_key_path: str = ""):
        # Timer for the date and time label
        self.timer = QtCore.QTimer()  # Create a timer object
        self.timer.timeout.connect(self.job_update)  # Assign the timeout signal to date and time show
        # self.timer.timeout.connect(self.finished_job_update)
        self.timer.setInterval(1000)  # Update date and time ever second

        hostname = self.cfg_data.get_hostname()  # Get the hostname from the settings
        if password == "":
            password = None

        self.connectionStatusSignal.emit("Connecting")
        try:
            if priv_key_path == "":
                self.ssh_client.connect(hostname, username=username, password=password)
            else:
                self.ssh_client.connect(hostname, username=username, password=password, key_filename=priv_key_path,
                                        look_for_keys=False)
            self.connectionStatusSignal.emit("Connected")
            self.timer.start()
        except ssh_exception.BadAuthenticationType:
            self.errorDialogMessageSignal.emit("There was a problem authenticating.\nCheck you credentials again.")
            self.connectionStatusSignal.emit("Disconnected")
            self.logger.exception("SSH Authentication error")
        except ssh_exception.PasswordRequiredException:
            self.errorDialogMessageSignal.emit("SSH password is required.")
            self.connectionStatusSignal.emit("Disconnected")
            self.logger.exception("SSH password is required")
        except FileNotFoundError:
            self.errorDialogMessageSignal.emit("Provided Private key file not found")
            self.connectionStatusSignal.emit("Disconnected")
            self.logger.exception("Private key file not found.")
        except IsADirectoryError:
            self.errorDialogMessageSignal.emit("A private key should a file, not a directory!")
            self.connectionStatusSignal.emit("Disconnected")
            self.logger.exception("Directory provided as private key.")

    # todo Add the destination directory path choice
    @QtCore.pyqtSlot(str, str, name='SSHFileTransportSignal')
    def send_files(self, local_path: str, remote_path: str):
        with self.ssh_client.open_sftp() as sftp_client:
            os.chdir(os.path.split(local_path)[0])
            parent = os.path.split(local_path)[1]
            try:
                sftp_client.mkdir(remote_path.replace("\\", "/"))
            except OSError:
                pass  # Directory exists
            for walker in os.walk(parent):
                try:
                    sftp_client.mkdir(os.path.join(remote_path, walker[0]).replace("\\", "/"))
                except OSError:
                    pass
                for file in walker[2]:
                    sftp_client.put(os.path.join(walker[0], file), os.path.join(remote_path,
                                                                                walker[0], file).replace("\\", "/"))

    @QtCore.pyqtSlot(str, str, name='getSFTPFilesSignal')
    def get_files(self, local_path: str, remote_path: str):
        scratch_dir, stderr = self.command_execution("echo -n ${SCRATCHDIR}")

        if stderr == "":
            remote_path = os.path.join(scratch_dir, remote_path).replace("\\", "/")
            print(remote_path)

            with self.ssh_client.open_sftp() as sftp_client:
                for path, files in self.sftp_walk(remote_path, sftp_client):
                    try:
                        os.mkdir(os.path.join(local_path, path.replace(scratch_dir + "/", "")))
                    except FileExistsError:
                        pass

                    for file in files:
                        sftp_client.get(os.path.join(path, file).replace("\\", "/"),
                                        os.path.join(local_path, path.replace(scratch_dir + "/", ""), file))
        else:
            self.logger.error("There was an error getting the files.\n%s", stderr)

    def sftp_walk(self, remote_path: str, sftp_channel):
        path = remote_path
        files = []
        folders = []
        for f in sftp_channel.listdir_attr(remote_path):
            if S_ISDIR(f.st_mode):
                folders.append(f.filename)
            else:
                files.append(f.filename)
        if files:
            yield path, files
        for folder in folders:
            new_path = os.path.join(remote_path, folder).replace("\\", "/")
            for x in self.sftp_walk(new_path, sftp_channel):
                yield x

    @QtCore.pyqtSlot(str, name='commandExecutionSignal')
    def command_execution(self, command: str):
        stdin, stdout, stderr = self.ssh_client.exec_command(command)

        stdout = stdout.read().decode('utf-8')
        stderr = stderr.read().decode('utf-8')

        if stderr != "":
            self.errorDialogMessageSignal.emit("<html><head/><body><p align=\"center\"><span style = \""
                                               "font-weight:600\" style = \"color:#ff0000;\">"
                                               "Command execution error:</span></p></body></html>"
                                               "\n<html><head/><body><p><span style = "
                                               "\"font-style:italic\" style = \""
                                               "color:#ffb000\">%s</span></p></body></html>" % stderr)
            self.logger.error("Command could not be executed remotely.\n%s", stderr)
        return stdout, stderr

    @QtCore.pyqtSlot(str, str, name='sendFileSignal')
    def send_file(self, local_file_path: str, remote_file_path: str):
        with self.ssh_client.open_sftp() as sftp_client:
            sftp_client.put(local_file_path, remote_file_path)

    def job_update(self):
        stdout, stderr = self.command_execution("qstat | awk {'print $1 \" \" $2 \" \" $5'} | grep "
                                                "\"cream\"")

        if stderr == "":
            elements = stdout.replace("\n", " ").split(" ")  # Get the individual fields

            job_ids = [job_id.split(".")[0] for job_id in elements[0:-1:3]]
            app_names = elements[1:-1:3]
            job_statuses = elements[2:-1:3]
            jobs = self.job_status_parser([job_ids, app_names, job_statuses])

            self.jobInsertionSignal.emit(jobs, "update")
        else:
            self.logger.error("An error occurred while trying to update jobs.\n%s", stderr)
            self.timer.timeout.disconnect(self.job_update)

    def finished_job_update(self):
        log_file, stderr = self.command_execution("cd ${SCRATCHDIR} && find . -name \"*.cream*.log\"")

        if stderr == "":
            if log_file == "":
                error_file, stderr = self.command_execution("cd ${SCRATCHDIR} && find . -name \"*.o*\"")
                paths_splitter = error_file
            else:
                paths_splitter = log_file

            paths = paths_splitter.replace("./", "").split("\n")
            paths.remove('')

            if log_file != "":
                jobs = [file_name.split(".")[0] for file_name in [file.split("/")[-1] for file in paths]]
            else:
                jobs = [file_name.replace("o", "-").split("-")[-1] for file_name in [file.split("/")[-1] for file in
                                                                                     paths]]
            job_status = []
            job_size = []

            for path in paths:
                file_content, stderr = self.command_execution("cat ${SCRATCHDIR}/" + path)

                if stderr != "":
                    self.logger.error("Error while reading the file.\n%s", stderr)
                    # self.timer.timeout.disconnect(self.finished_job_update)
                    break

                if file_content.find("Normal completion of simulation on server") != -1:
                    job_status.append("Finished")  # Append a successful status
                    dir_size, stderr = self.command_execution("du -sh ${SCRATCHDIR}/" + path.split("/")[0])

                    if stderr == "":
                        job_size.append(dir_size.split("\t")[0])  # Write the job size
                    else:
                        job_size.append("0")  # Append zero size
                        self.logger.error("Error getting the job size.\n%s", stderr)
                        # self.timer.timeout.disconnect(self.finished_job_update)
                        break
                else:
                    job_status.append("Error")
                    job_size.append("0")
            self.jobInsertionSignal.emit([jobs, job_size, paths, job_status], "finished")
        else:
            self.logger.error("An error occurred while trying to update finished jobs.\n%s", stderr)
            # self.timer.timeout.disconnect(self.finished_job_update)

    @QtCore.pyqtSlot(list, name='finishedJobDeletionSignal')
    def finished_job_deletion(self, paths_to_delete: list):
        for path in paths_to_delete:
            self.command_execution("cd ${SCRATCHDIR} && rm -rf " + path)

    @staticmethod
    def job_status_parser(jobs: list):
        jobs[2] = [status.replace("R", "Running") for status in jobs[2]]
        jobs[2] = [status.replace("Q", "Queued") for status in jobs[2]]
        jobs[2] = [status.replace("E", "Error") for status in jobs[2]]
        return jobs

    def close(self):
        self.ssh_client.close()
        self.connectionStatusSignal.emit("Disconnected")

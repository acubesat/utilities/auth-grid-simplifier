# Strings of the default file settings are included here

LOG_CONFIG_DEFAULT = """Logging:
  version: 1
  disable_existing_loggers: True

  formatters:
    mainFile:
      format: "%(asctime)s  [%(name)35s.%(funcName)-20s]  %(levelname)-8s -  %(message)s"

  handlers:
    mainHandler:
      class: Core.Handlers.CustomLogHandler.CustomLogRotationHandler
      formatter: mainFile
      level: INFO
      filename: logs/SimpleGrid.log
      max_bytes: 10240  # Set change size to 1MB
      backup_count: 7  # Keep 7 days old files and delete older
      enc: 'utf-8'  # Set the file encoding to utf-8

  # Configure the root logger
  root:
    level: WARNING
    handlers: [mainHandler]
"""

SETTINGS_XML_DEFAULT = """<settings>
    <SSH port="22" />
    <RemoteHost>
        <hostname>ui.afroditi.hellsagrid.gr</hostname>
    </RemoteHost>
</settings>
"""

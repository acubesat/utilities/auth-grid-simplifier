#!/usr/bin/env python3

import os
import sys
import logging
import logging.config
sys.path.append(os.path.abspath('.'))  # noqa

# pylint: disable=wrong-import-position

import yaml
from PyQt5 import QtWidgets, QtCore, QtGui
from Core.GUI import UInterface
from Core.Handlers import SSHHandler, OperationsHandler, CustomLogHandler
from Core.Configuration import DefaultData, ConfigData

# pylint: disable=wrong-import-position


def parse_files():
    # Create the directory for the log files if it does not exist already
    try:
        if not os.path.exists('logs'):
            os.makedirs('logs')  # Create the logs directory
        if not os.path.exists('Settings'):
            os.makedirs('Settings')  # Create the settings directory
    except Exception as exception:
        print("There is a problem creating directories. See tracback: \n%s" % exception, file=sys.stderr)
        sys.exit(-1)  # Exit the program if an error occurred

    # Check if the logging configuration file exists
    try:
        if not os.path.exists(os.path.abspath('Settings/logging.yml')):
            print("Logging configuration file not found. Creating the default.", file=sys.stderr)
            log_file = open(os.path.abspath('Settings/logging.yml'), "w+")  # Open file in writing mode
            log_file.write(DefaultData.LOG_CONFIG_DEFAULT)  # Write the default dat to the file
            log_file.close()  # Close the file, since no other operation required
    except Exception as exception:
        print("There is a problem creating the configuration file. See tracback: \n%s" % exception, file=sys.stderr)
        sys.exit(-1)  # Exit the program if an error occurred

    # Check if the settings XML file exists
    try:
        if not os.path.exists(os.path.abspath('Settings/settings.xml')):
            print("Settings file not found. Creating the default.", file=sys.stderr)
            setngs_file = open(os.path.abspath('Settings/settings.xml'), "w+")  # Open the settings file in writing mode
            setngs_file.write(DefaultData.SETTINGS_XML_DEFAULT)  # Write the default dat to the file
            setngs_file.close()  # Close the file, since no other operation required
    except Exception as exception:
        print("There is a problem creating the settings file. See tracback: \n%s" % exception, file=sys.stderr)
        sys.exit(-1)  # Exit the program if an error occurred

    # Open the configuration and apply it on the logging module
    with open(os.path.abspath('Settings/logging.yml')) as config_file:
        dictionary = yaml.safe_load(config_file)  # Load the dictionary configuration
        logging.config.dictConfig(dictionary['Logging'])  # Select the logging settings from the dictionary


def main():
    log_data = logging.getLogger(__name__)  # Create the logger for the program

    # Set the GUI Font according to the OS used
    font = QtGui.QFont()  # Create the font object
    font.setStyleHint(QtGui.QFont.Monospace)  # Set the a default font in case some of the following is not found
    if sys.platform.startswith('linux'):
        font.setFamily("Ubuntu")  # Set the font for Ubuntu/linux
        font.setPointSize(11)
    elif sys.platform.startswith('win32'):
        font.setFamily("Segoe UI")  # Set the font for Windows
        font.setPointSize(8)

    app = QtWidgets.QApplication(sys.argv)
    app.setFont(font)

    # Exception handling code for the XML file process
    try:
        configuration = ConfigData.ConfData(os.path.abspath('Settings/settings.xml'))
    except Exception:
        log_data.exception("There is a problem with the XML file handling. Program terminates.")
        sys.exit(1)  # Terminate the script

    ui = UInterface.UISimpleGrid(configuration)  # Instantiate the GUI

    ssh_handler_thread = QtCore.QThread()
    ssh_handler = SSHHandler.SSHHandler(configuration)
    ssh_handler.moveToThread(ssh_handler_thread)
    # ssh_handler_thread.started.connect()
    ssh_handler_thread.finished.connect(ssh_handler.close)

    operation_handler_thread = QtCore.QThread()  # Create a thread for the operation handler
    operation_handle = OperationsHandler.OperationHandler(ssh_handler, ssh_handler_thread, ui, configuration)
    operation_handle.moveToThread(operation_handler_thread)  # Move the operation handler to a thread
    operation_handler_thread.started.connect(operation_handle.start)  # Run the start method upon thread start
    operation_handler_thread.finished.connect(operation_handle.app_exit_request)  # Close the app on handler exit
    operation_handler_thread.finished.connect(operation_handle.deleteLater)  # Wait until the thread has stopped
    operation_handler_thread.finished.connect(operation_handler_thread.deleteLater)  # Delete the thread before exiting
    operation_handler_thread.start()  # Start the operations handler

    app.aboutToQuit.connect(operation_handler_thread.quit)

    ui.show_application()  # Render and show the GUI main window and start the application
    sys.exit(app.exec_())  # Execute the app until exit is selected


if __name__ == "__main__":
    parse_files()  # Parse the necessary files. All error checking is included
    log = logging.getLogger(__name__)  # Create the logger for the program

    try:
        # Redirect sterr to the logging file
        std_err_logger = logging.getLogger('STDERR')  # Create an STDERR logger
        sys.stderr = CustomLogHandler.StreamToLogger(std_err_logger, logging.ERROR)  # Redirect stderr to the logger
        main()  # Run the main program
    except (Exception, OSError):
        log.exception("An major error occurred. See the traceback below.")

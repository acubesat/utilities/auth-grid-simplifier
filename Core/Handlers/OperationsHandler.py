import os
import logging
from PyQt5 import QtCore


class OperationHandler(QtCore.QObject):
    def __init__(self, ssh_handler, ssh_handler_thread, ui, cfg_data):
        super(OperationHandler, self).__init__(None)

        self.ssh_handler = ssh_handler
        self.ssh_handler_thread = ssh_handler_thread
        self.cfg_data = cfg_data
        self.ui = ui

        self.logger = logging.getLogger(__name__)  # Data logger object

    def start(self):
        self.signal_connections()
        self.ssh_handler_thread.start()

    def client_connection_request(self):
        if self.ui.main_widget.clientConnectionButton.text() == "Connect":
            username = self.ui.main_widget.userNameEntry.text()
            password = self.ui.main_widget.userPasswordEntry.text()

            if username != "":
                self.ssh_handler.clientConnectionSignal.emit(username, password,
                                                             self.ui.main_widget.privateKeyPathEnrty.text())
            self.ui.setEmptyBoxSignal.emit("Connection")
        else:
            self.ssh_handler.closeClientConnectionSignal.emit()

    def job_submission(self):
        local_path = self.ui.main_widget.projectFileEntry.text()
        remote_path = self.ui.main_widget.remoteProjectPath.text()

        if remote_path == "":
            remote_path = "/home/" + self.ui.main_widget.userNameEntry.text() + "/Projects/"
        remote_path.replace("\\", "/")

        if local_path != "":
            local_dir_base = os.path.split(local_path)[-1]
            self.ssh_handler.sendFilesSignal.emit(local_path, remote_path)
            solver = self.ui.main_widget.solverSelectionComboBox.currentText()  # Get the selected solver

            if solver == "Electromagnetics":
                script_name = 'electromagnetics.sh'
                self.ssh_handler.sendFileSignal.emit(os.path.join(os.path.abspath('./Solver Scripts'), script_name),
                                                     os.path.join(remote_path, local_dir_base, script_name))
            exec_command = "cd ${SCRATCHDIR} && cd " + local_dir_base + " && qsub " + script_name
            self.ssh_handler.commandExecutionSignal.emit("cp -r " + remote_path + local_dir_base +
                                                         " ${SCRATCHDIR} && " + exec_command)
        self.ui.setEmptyBoxSignal.emit("Project path")

    @QtCore.pyqtSlot(str, name='deleteJobsSignal')
    def job_deletion(self, job: str):
        self.ssh_handler.commandExecutionSignal.emit("qdel " + job)

    def signal_connections(self):
        self.ui.main_widget.clientConnectionButton.clicked.connect(self.client_connection_request)
        self.ui.main_widget.jobSubmissionButton.clicked.connect(self.job_submission)
        self.ui.main_widget.remoteJobUpdateButton.clicked.connect(self.ssh_handler.job_update)
        self.ui.main_widget.finishedJobsUpdateButton.clicked.connect(self.ssh_handler.finished_job_update)
        self.ui.sendFilesUISignal.connect(self.ssh_handler.send_file)
        self.ui.saveSettingsSig.connect(self.save_settings_action)
        self.ui.deleteJobsSignal.connect(self.job_deletion)
        self.ui.getSFTPFilesSignal.connect(self.ssh_handler.get_files)
        self.ui.finishedJobDeletionSignal.connect(self.ssh_handler.finished_job_deletion)

        self.ssh_handler.connectionStatusSignal.connect(self.ui.client_connection_status)
        self.ssh_handler.errorDialogMessageSignal.connect(self.ui.ssh_error)
        self.ssh_handler.jobInsertionSignal.connect(self.ui.job_insertion)

    def save_settings_action(self):
        self.ssh_handler.closeClientConnectionSignal.emit()

        ssh_port = self.ui.settings_widget.sshPortEntry.value()
        hostname = self.ui.settings_widget.remoteHostEntry.text()

        self.cfg_data.set_ssh_port(ssh_port)
        self.cfg_data.set_hostname(hostname)

    def app_exit_request(self):
        self.ssh_handler_thread.quit()
        self.ssh_handler_thread.wait()
        self.ssh_handler.deleteLater()
        self.ssh_handler_thread.deleteLater()

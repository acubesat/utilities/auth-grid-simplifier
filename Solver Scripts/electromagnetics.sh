#!/usr/bin/env bash

#PBS -N hfss
#PBS -q see
#PBS -j oe
#PBS -l nodes=1:ppn=8,walltime=1:00:00

cd ${PBS_O_WORKDIR}
# Change HFSS version info untill the new one is installed
sed -i.bak "s/Version(2019, 1)/Version(2018, 1)/" *.aedt

# Load the ansys electromagnetic solver
module load AnsysEM/191
time ansysedt -distribute -machinelist num=${PBS_NP} -BatchSolve -ng -Logfile ${PBS_JOBID}.log -batchoptions "HFSS/HPCLicenseType=pool" *.aedt

import xml.etree.ElementTree as ETree
import logging


class ConfData:
    # Class constructor
    def __init__(self, filename):
        self.filename = filename  # Create a variable with the given filename
        self.logger = logging.getLogger(__name__)  # Create the logger for this module
        try:
            self.tree = ETree.parse(self.filename)  # Try to parse the given file
            self.root = self.tree.getroot()  # Get the root from the XML file
        except ETree.ParseError:
            self.logger.exception("There is an issue with the XML settings file. See traceback below.")

    def parse(self):
        try:
            self.tree = ETree.parse(self.filename)  # Try to parse the given file
            self.root = self.tree.getroot()  # Get the root from the XML file
        except ETree.ParseError:
            self.logger.exception("There is an issue with the XML settings file. See traceback below.")

    def get_config(self, child, sub_child):
        children = list(self.root.find(child))
        for item in children:
            if item.tag == sub_child:
                return item.text
        return ""

    def set_config(self, element, child, value):
        elm = self.root.find(element)  # Get the required element from the tree
        children = list(elm)  # List the children of the element
        for item in children:
            if item.tag == child:
                item.text = value
                self.tree.write(self.filename)
                break
            else:
                continue

    def get_hostname(self):
        return self.get_config("RemoteHost", "hostname")

    def set_hostname(self, hostname: str):
        self.set_config("RemoteHost", "hostname", hostname)

    def get_ssh_port(self):
        return self.root.find("SSH").get("port")

    def set_ssh_port(self, ssh_port):
        self.root.find("SSH").set("port", str(ssh_port))

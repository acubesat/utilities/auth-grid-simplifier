import os
import sys
import logging

from PyQt5 import QtCore, QtWidgets, uic


class UISimpleGrid(QtCore.QObject):
    connectClientUISignal = QtCore.pyqtSignal(str, str, name='ClientConnectionSignal')
    sendFilesUISignal = QtCore.pyqtSignal(str, str, name='ClientFileTransportSignal')
    setEmptyBoxSignal = QtCore.pyqtSignal(str, name='styleSetterSignal')
    saveSettingsSignal = QtCore.pyqtSignal(name='saveSettingsSig')
    deleteJobsSignal = QtCore.pyqtSignal(str, name='deleteJobsSignal')
    finishedJobDeletionSignal = QtCore.pyqtSignal(list, name='finishedJobDeletionSignal')

    getSFTPFilesSignal = QtCore.pyqtSignal(str, str, name='getSFTPFilesSignal')

    def __init__(self, cfg_data):
        """
        Render the GUI and connect the necessary signals
        Args:
            cfg_data: XML object configuration data
        """
        super(UISimpleGrid, self).__init__(parent=None)

        # Load the resources binary
        resource_object = QtCore.QResource()
        resource_file = resource_object.registerResource(os.path.abspath("Core/GUI/resources.rcc"))
        if resource_file.bit_length() == 0:
            sys.exit(-1)  # Indicate an error when exiting

        self.logger = logging.getLogger(__name__)  # Create the logger for the file

        self.main_window = QtWidgets.QMainWindow()  # Create the main window instance
        self.settings_dialog = QtWidgets.QDialog(parent=self.main_window)

        self.main_widget = self.ui_loader(':/UI_Files/MainWindow', self.main_window)
        self.settings_widget = self.ui_loader(':/UI_Files/Settings', self.settings_dialog)

        # Necessary signal connections
        self.main_widget.openProjectButton.clicked.connect(self.project_directory_getter)
        self.main_widget.privateKeyDirectoryButton.clicked.connect(self.priv_key_directory_getter)

        self.main_widget.actionOpen_Project.triggered.connect(self.project_directory_getter)
        self.main_widget.actionSettings.triggered.connect(self.show_settings_dialog)
        self.main_widget.actionExit.triggered.connect(self.close_application)
        self.settings_widget.saveSettingsButtonBox.accepted.connect(self.save_settings_warning)
        self.main_widget.jobDeletionButton.clicked.connect(self.job_deletion_warning)

        self.main_widget.userNameEntry.returnPressed.connect(self.main_widget.clientConnectionButton.click)
        self.main_widget.userPasswordEntry.returnPressed.connect(self.main_widget.clientConnectionButton.click)

        self.main_widget.downloadJobButton.clicked.connect(self.save_remote_job)
        self.main_widget.deleteFinishedJobButton.clicked.connect(self.finished_job_deletion_ui)

        self.setEmptyBoxSignal.connect(self.style_setter_empty_box)
        self.cfg_data = cfg_data
        QtWidgets.QApplication.setStyle(QtWidgets.QStyleFactory.create("Fusion"))  # Change the style of the GUI

    @QtCore.pyqtSlot(str, name='ConnectionStatusSignal')
    def client_connection_status(self, data: str):
        """
        Client connection status indication
        Args:
            data: Selection string
        """
        if data == "Connecting":
            self.main_widget.clientConnectionButton.setText("Stop")  # Change user's selection
            self.main_widget.jobSubmissionButton.setEnabled(False)  # Disable job submission
            self.main_widget.jobDetailsTreeWidget.setEnabled(False)
            self.main_widget.finishedJobsTreeWidget.setEnabled(False)
            self.main_widget.downloadJobButton.setEnabled(False)
            self.main_widget.deleteFinishedJobButton.setEnabled(False)
            self.main_widget.remoteJobUpdateButton.setEnabled(False)
            self.main_widget.finishedJobsUpdateButton.setEnabled(False)
            self.main_widget.jobDetailsTreeWidget.clear()
            self.main_widget.connectionStatusLabel.setText("<html><head/><body><p><span style=\" font-weight:600;"
                                                           "color:#ffb400;\">Connecting...</span></p></body></html>")
        elif data == "Connected":
            self.main_widget.clientConnectionButton.setText("Disconnect")
            self.main_widget.jobSubmissionButton.setEnabled(True)
            self.main_widget.jobDetailsTreeWidget.setEnabled(True)
            self.main_widget.finishedJobsTreeWidget.setEnabled(True)
            self.main_widget.downloadJobButton.setEnabled(True)
            self.main_widget.deleteFinishedJobButton.setEnabled(True)
            self.main_widget.remoteJobUpdateButton.setEnabled(True)
            self.main_widget.finishedJobsUpdateButton.setEnabled(True)
            self.main_widget.jobDetailsTreeWidget.clear()
            self.main_widget.connectionStatusLabel.setText("<html><head/><body><p><span style=\" font-weight:600;"
                                                           "color:#00ff00;\">Connected</span></p></body></html>")
        elif data == "Disconnected":
            self.main_widget.clientConnectionButton.setText("Connect")  # Change user's selection
            self.main_widget.jobSubmissionButton.setEnabled(False)
            self.main_widget.jobDetailsTreeWidget.setEnabled(False)
            self.main_widget.finishedJobsTreeWidget.setEnabled(False)
            self.main_widget.downloadJobButton.setEnabled(False)
            self.main_widget.deleteFinishedJobButton.setEnabled(False)
            self.main_widget.remoteJobUpdateButton.setEnabled(False)
            self.main_widget.finishedJobsUpdateButton.setEnabled(False)
            self.main_widget.jobDetailsTreeWidget.clear()
            self.main_widget.connectionStatusLabel.setText("<html><head/><body><p><span style=\" font-weight:600;"
                                                           "color:#ff0000;\">Disconnected</span></p></body></html>")

    @staticmethod
    def ui_loader(ui_resource, base=None):
        ui_file = QtCore.QFile(ui_resource)
        ui_file.open(QtCore.QFile.ReadOnly)
        try:
            parsed_ui = uic.loadUi(ui_file, base)
            return parsed_ui
        finally:
            ui_file.close()

    def project_directory_getter(self):
        options = QtWidgets.QFileDialog.Options()
        options |= QtWidgets.QFileDialog.DontUseNativeDialog
        options |= QtWidgets.QFileDialog.ShowDirsOnly
        directory = str(QtWidgets.QFileDialog.getExistingDirectory(self.main_widget, "Select Directory",
                                                                   options=options))
        self.main_widget.projectFileEntry.setText(directory)

    def priv_key_directory_getter(self):
        options = QtWidgets.QFileDialog.Options()
        options |= QtWidgets.QFileDialog.DontUseNativeDialog
        file = QtWidgets.QFileDialog.getOpenFileName(self.main_widget, "Select private key file", options=options)
        self.main_widget.privateKeyPathEnrty.setText(file[0])

    def save_remote_job(self):
        jobs_to_save = self.main_widget.finishedJobsTreeWidget.selectedItems()

        if jobs_to_save:
            options = QtWidgets.QFileDialog.Options()
            options |= QtWidgets.QFileDialog.DontUseNativeDialog
            options |= QtWidgets.QFileDialog.ShowDirsOnly
            options |= QtWidgets.QFileDialog.AcceptSave
            path = QtWidgets.QFileDialog.getExistingDirectory(self.main_widget, "Save the finished job",
                                                              options=options)

            for job in jobs_to_save:
                self.getSFTPFilesSignal.emit(path, job.text(3).split("/")[0])

    @QtCore.pyqtSlot(str, name='styleSetterSignal')
    def style_setter_empty_box(self, entry: str):
        # Make boxes red wherever there is no input
        if entry == "Connection":
            if self.main_widget.userNameEntry.text() == "":
                self.main_widget.userNameEntry.setStyleSheet("border: 1px solid red")
            else:
                self.main_widget.userNameEntry.setStyleSheet("")
        elif entry == "Project path":
            if self.main_widget.projectFileEntry.text() == "":
                self.main_widget.projectFileEntry.setStyleSheet("border: 1px solid red")
            else:
                self.main_widget.projectFileEntry.setStyleSheet("")

    @QtCore.pyqtSlot(list, str, name='jobInsertionSignal')
    def job_insertion(self, jobs: list, insertion_type: str):
        if insertion_type == "update":
            items = {}  # Create an empty dictionary to hold the items in
            for i in range(0, self.main_widget.jobDetailsTreeWidget.topLevelItemCount()):
                items[self.main_widget.jobDetailsTreeWidget.topLevelItem(i).text(0)] = i
            app_name_dictionary = dict(zip(jobs[0], jobs[1]))
            status_dictionary = dict(zip(jobs[0], jobs[2]))

            for job in items.keys():
                try:
                    if self.main_widget.jobDetailsTreeWidget.topLevelItem(items[job]).text(1) != \
                            status_dictionary[job]:
                        self.main_widget.jobDetailsTreeWidget.topLevelItem(items[job]).setText(1,
                                                                                               status_dictionary[job])
                except KeyError:
                    self.logger.exception("Remote directory possible problem. See traceback below.")
                    pass

            local_generator = (job for job in jobs[0] if job not in items)
            remote_generator = (job for job in items if job not in jobs[0])

            for job in remote_generator:
                self.main_widget.jobDetailsTreeWidget.takeTopLevelItem(items[job])

            for job in local_generator:
                widget_item = QtWidgets.QTreeWidgetItem(self.main_widget.jobDetailsTreeWidget,
                                                        [job, status_dictionary[job], app_name_dictionary[job]])
                self.main_widget.jobDetailsTreeWidget.addTopLevelItem(widget_item)
        elif insertion_type == "finished":
            items = {}  # Create an empty dictionary to hold the items in
            for i in range(0, self.main_widget.finishedJobsTreeWidget.topLevelItemCount()):
                items[self.main_widget.finishedJobsTreeWidget.topLevelItem(i).text(0)] = i
            job_size_dictionary = dict(zip(jobs[0], jobs[1]))
            job_paths_dictionary = dict(zip(jobs[0], jobs[2]))
            job_status_dictionary = dict(zip(jobs[0], jobs[3]))

            for job in items.keys():
                try:
                    if self.main_widget.finishedJobsTreeWidget.topLevelItem(items[job]).text(1) != \
                            job_status_dictionary[job]:
                        self.main_widget.finishedJobsTreeWidget.topLevelItem(items[job]).\
                            setText(1, job_status_dictionary[job])
                except KeyError:
                    self.logger.exception("Remote directory possible problem. See traceback below.")
                    pass

            local_generator = (job for job in jobs[0] if job not in items)
            remote_generator = (job for job in items if job not in jobs[0])

            for job in remote_generator:
                self.main_widget.finishedJobsTreeWidget.takeTopLevelItem(items[job])

            for job in local_generator:
                widget_item = QtWidgets.QTreeWidgetItem(self.main_widget.finishedJobsTreeWidget,
                                                        [job, job_status_dictionary[job], job_size_dictionary[job],
                                                         job_paths_dictionary[job]])
                self.main_widget.finishedJobsTreeWidget.addTopLevelItem(widget_item)

    def finished_job_deletion_ui(self):
        jobs_to_delete = self.main_widget.finishedJobsTreeWidget.selectedItems()

        if jobs_to_delete:
            choice = QtWidgets.QMessageBox.warning(self.main_widget, 'Delete job(s)',
                                                   "<html><head/><body><p align=\"center\"><span style = \""
                                                   "font-weight:600\" style = \"color:#ff0000;\">"
                                                   "You are about to execute a destructive command to delete a "
                                                   "job!</span></p></body></html>"
                                                   "\n<html><head/><body><p><span style = "
                                                   "\"font-style:italic\" style = \""
                                                   "color:#ffb000\">Are you sure you want to continue?"
                                                   "</span></p></body></html>",
                                                   QtWidgets.QMessageBox.Yes | QtWidgets.QMessageBox.No,
                                                   QtWidgets.QMessageBox.No)

            if choice == QtWidgets.QMessageBox.Yes:
                paths = [path.text(3).split("/")[0] for path in jobs_to_delete]
                self.finishedJobDeletionSignal.emit(paths)
        else:
            QtWidgets.QMessageBox.information(self.main_widget, 'Delete job(s)',
                                              "<html><head/><body><p align=\"center\"><span style = \""
                                              "font-weight:600\" style = \"color:#3FBF3F;\">"
                                              "No job selected.</span></p></body></html>",
                                              QtWidgets.QMessageBox.Ok, QtWidgets.QMessageBox.Ok)

    def show_application(self):
        self.main_widget.show()  # Show the GUI window

    def ssh_error(self, message: str):
        QtWidgets.QMessageBox.warning(self.main_widget, 'SSH Error', message,
                                      QtWidgets.QMessageBox.Ok, QtWidgets.QMessageBox.Ok)

    def save_settings_warning(self):
        choice = QtWidgets.QMessageBox.warning(self.settings_widget, 'Save settings',
                                               "<html><head/><body><p align=\"center\"><span style = \""
                                               "font-weight:600\" style = \"color:#FFAA00;\">"
                                               "Do you really want to save the settings?</span></p></body></html>",
                                               QtWidgets.QMessageBox.Yes | QtWidgets.QMessageBox.No,
                                               QtWidgets.QMessageBox.No)

        if choice == QtWidgets.QMessageBox.Yes:
            self.saveSettingsSignal.emit()  # Send the saving signal

    def job_deletion_warning(self):
        jobs_to_delete = self.main_widget.jobDetailsTreeWidget.selectedItems()

        if jobs_to_delete:
            choice = QtWidgets.QMessageBox.warning(self.main_widget, 'Delete job(s)',
                                                   "<html><head/><body><p align=\"center\"><span style = \""
                                                   "font-weight:600\" style = \"color:#ff0000;\">"
                                                   "You are about to execute a destructive command to delete a "
                                                   "job!</span></p></body></html>"
                                                   "\n<html><head/><body><p><span style = "
                                                   "\"font-style:italic\" style = \""
                                                   "color:#ffb000\">Are you sure you want to continue?"
                                                   "</span></p></body></html>",
                                                   QtWidgets.QMessageBox.Yes | QtWidgets.QMessageBox.No,
                                                   QtWidgets.QMessageBox.No)

            if choice == QtWidgets.QMessageBox.Yes:
                for job in jobs_to_delete:
                    self.deleteJobsSignal.emit(job.text(0))  # Send the deletion signal
        else:
            QtWidgets.QMessageBox.information(self.main_widget, 'Delete job(s)',
                                              "<html><head/><body><p align=\"center\"><span style = \""
                                              "font-weight:600\" style = \"color:#3FBF3F;\">"
                                              "No job selected.</span></p></body></html>",
                                              QtWidgets.QMessageBox.Ok, QtWidgets.QMessageBox.Ok)

    def show_settings_dialog(self):
        ssh_port = self.cfg_data.get_ssh_port()
        hostname = self.cfg_data.get_hostname()

        self.settings_widget.sshPortEntry.setValue(int(ssh_port))
        self.settings_widget.remoteHostEntry.setText(hostname)

        self.settings_widget.show()

    def close_application(self):
        choice = QtWidgets.QMessageBox.question(self.main_widget, 'Exit', "Are you sure?",
                                                QtWidgets.QMessageBox.Yes | QtWidgets.QMessageBox.No,
                                                QtWidgets.QMessageBox.No)
        if choice == QtWidgets.QMessageBox.Yes:
            QtCore.QCoreApplication.quit()  # If user selects "Yes", then exit from the application
